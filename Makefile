TARGET ?= armv7-unknown-linux-gnueabihf
IMAGE_REPO = registry.gitlab.com/modioab/modio-mqttd
IMAGE_FILES += target/$(TARGET)/release/modio-mqttd
IMAGE_FILES += config/rumqttd.conf
IMAGE_FILES += config/modio-mqttd.service
IMAGE_FILES += config/modio-mqttd.path
IMAGE_FILES += config/hosts
IMAGE_FILES += config/machine-id
IMAGE_FILES += config/resolv.conf

CLEANUP_FILES += config/hosts 
CLEANUP_FILES += config/machine-id
CLEANUP_FILES += config/resolv.conf

include build.mk


config/hosts config/machine-id config/resolv.conf:
	touch $@
