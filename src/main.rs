// Author: D.S. Ljungmark <spider@skuggor.se>, Modio AB
// SPDX-License-Identifier: AGPL-3.0-or-later
use argh::FromArgs;
use librumqttd::{Broker, Config};
use log::{error, info};
use std::path::PathBuf;

/* #[cfg(not(target_env = "msvc"))]
#[global_allocator]
static ALLOC: jemallocator::Jemalloc = jemallocator::Jemalloc;
*/

#[derive(FromArgs, Debug)]
/// Command line args for rumqttd
struct CommandLine {
    /// path to config file
    #[argh(option, short = 'c')]
    config: PathBuf,
    /// verbosity
    ///
    /// Add more -v -v -v in order to get a higher log level.
    #[argh(switch, short = 'v')]
    verbose: u32,
}

const fn verbosity(verbose: u32) -> log::LevelFilter {
    match verbose {
        0 => log::LevelFilter::Error,
        1 => log::LevelFilter::Warn,
        2 => log::LevelFilter::Info,
        3 => log::LevelFilter::Debug,
        _ => log::LevelFilter::Trace,
    }
}

fn main() {
    // Parse commandline options
    let commandline: CommandLine = argh::from_env();
    let _ = env_logger::Builder::new()
        .filter_level(verbosity(commandline.verbose))
        .format_timestamp_secs()
        .write_style(env_logger::WriteStyle::Auto)
        .init();
    info!("Commandline: {:#?}", commandline);
    let config: Config = confy::load_path(&commandline.config).unwrap();
    let res = Broker::new(config).start();
    error!("Stopping broker!! Error = {:?}", res);
}
