# modio-mqttd

Almost the same as upstream rumqttd, except disabling profiling, disabling TLS,
and configuring the logging via the commandline.

This isn't a solution I'm happy with, as it still has the Warp dependency for
the console, something I do not wish to have.

It also only supports hard-coded Listen addresses and not socket-activation,
another misfeature for the way we wish to use the MQTT broker.


## Configuration

The config file needs to match rumqttd config.
See:
	https://github.com/bytebeamio/rumqtt/tree/master/rumqttd
