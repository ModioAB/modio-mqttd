FROM --platform=linux/arm registry.gitlab.com/modioab/scada-hello/template:armhf

ARG URL=unknown
ARG COMMIT=unknown
ARG BRANCH=unknown
ARG HOST=unknown
ARG DATE=unknown
ARG TARGET=armv7-unknown-linux-gnueabihf

LABEL "se.modio.ci.url"=$URL "se.modio.ci.branch"=$BRANCH "se.modio.ci.commit"=$COMMIT "se.modio.ci.host"=$HOST "se.modio.ci.date"=$DATE "se.modio.service"=modio-mqttd

COPY config/rumqttd.conf  /etc/rumqttd.conf
COPY config/hosts /etc/hosts
COPY config/resolv.conf /etc/resolv.conf
COPY config/machine-id	/etc/machine-id
COPY config/modio-mqttd.service /etc/systemd/system/modio-mqttd.service
COPY config/modio-mqttd.path /etc/systemd/system/modio-mqttd.path
COPY target/${TARGET}/release/modio-mqttd /opt/bin/modio-mqttd
